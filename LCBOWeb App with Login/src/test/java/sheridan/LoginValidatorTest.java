package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		boolean result = LoginValidator.isValidLength("Trevor221");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean result = LoginValidator.isValidLength("#342S");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = LoginValidator.isValidLength("Trevo1");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = LoginValidator.isValidLength("Trevor1");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidStartOfLoginNameRegular() {
		boolean result = LoginValidator.isValidStartOfLoginName("Trevo1");
		assertTrue("Invalid Start", result);
	}
	
	@Test
	public void testIsValidStartOfLoginNameException() {
		boolean result = LoginValidator.isValidStartOfLoginName("7revo1");
		assertFalse("Invalid Start", result);
	}
	
	@Test
	public void testIsValidStartOfLoginNameBoundaryIn() {
		boolean result = LoginValidator.isValidStartOfLoginName("2");
		assertFalse("Invalid Start", result);
	}
	
	@Test
	public void testIsValidStartOfLoginNameBoundaryOut() {
		boolean result = LoginValidator.isValidStartOfLoginName("t");
		assertTrue("Invalid Start", result);
	}

}
