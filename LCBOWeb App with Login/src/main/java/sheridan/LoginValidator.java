package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return isValidLength(loginName) && isValidStartOfLoginName(loginName);
	}
	
	public static boolean isValidLength(String loginName) {
		return (loginName.length() >= 6) && (loginName.matches("[A-Za-z0-9]+"));
	}
	
	public static boolean isValidStartOfLoginName(String loginName) {
		return (!(Character.isDigit(loginName.charAt(0)))) && (loginName.matches("[A-Za-z0-9]+"));
	}
}
